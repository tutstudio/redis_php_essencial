# redis_php_essencial

This a redis example codes of different data type implementation.

to start.


```
#!composer

composer install
```


### Voting system: ###
In this example we will see how to work with string and hash data type.


```
#!php

php voting_system.php //to test redis string
php hash_voting_system.php //to test redis hash
```


### Queue system: ###
In this example we will see how to work with lists in redis.

In one terminal run **php queue_pusher.php** to push a data to a list.
In other terminal run **php queue_consumer.php** to pull data from the list

### Timeseries system: ###
In this example we can see a sample code to store user actions by seconds, minutes, hours...

```
#!php


php using_timeseries_unique.php string //the system use string data type redis
php using_timeseries_unique.php hash //the system use string data type redis

php using_timeseries.php sort //the system use sorted set
php using_timeseries.php hll //the system use hyperloglog data type
```